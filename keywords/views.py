import csv
import pandas as pd
from io import StringIO

from django.shortcuts import render
from django.views.generic import TemplateView


class KeywordView(TemplateView):
    template_name = 'base.html'
    
    def post(self, request, *args, **kwargs):
        file = request.FILES['file']       
        data_frame = pd.read_csv(file)
        data = data_frame.to_dict('records')
        top_list = []

        for x in data:
            try:
                if int(x['Position']) == 1 and x['Volume'] > 700:
                    top_list.append(x)
            except:
                pass

        if top_list:
            top_result = max(top_list, key=lambda x:x['Volume'])
            brand_name = top_result['Keyword']
            phrases = 0
            for x in data:
                if brand_name in x['Keyword']:
                    phrases += 1
        else:
            brand_name = "Not a brand"
            phrases = 0

        context = {
            'name': request.POST['name'],
            'brand_name': brand_name,
            'phrases': phrases
        }
        
        return render(request, 'base.html', context)
        